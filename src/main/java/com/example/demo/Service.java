/**
 * 
 */
package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author victo
 *
 */
@RestController
@RequestMapping("/author")
public class Service {

	@Autowired
	private AuthorRepository authorRepository;
	
	@GetMapping("/get")
	public List<Author> findAll() {
		return authorRepository.findAll();
	}
	
}
